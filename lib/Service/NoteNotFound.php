<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Thomas Clavier <tom@tcweb.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\CleanInstantUpload\Service;

class NoteNotFound extends \Exception {
}
