<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Thomas Clavier <tom@tcweb.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\CleanInstantUpload\AppInfo;

use OCP\AppFramework\App;

class Application extends App {
	public const APP_ID = 'cleaninstantupload';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}
}
