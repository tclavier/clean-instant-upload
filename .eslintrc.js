// SPDX-FileCopyrightText: Thomas Clavier <tom@tcweb.org>
// SPDX-License-Identifier: AGPL-3.0-or-later
module.exports = {
	extends: [
		'@nextcloud',
	]
}
